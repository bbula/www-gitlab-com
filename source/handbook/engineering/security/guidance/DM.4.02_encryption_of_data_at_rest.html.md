---
layout: markdown_page
title: "DM.4.02 - Encryption of Data at Rest Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# DM.4.02 - Encryption of Data at Rest

## Control Statement

Red, orange, and yellow data at rest is encrypted.

## Context

Encrypting data at rest helps ensure the confidentiality and integrity of that data. In the event a production instance, data store, or backup is compromised, without encryption, the data is near certain to be accessed, modified, and/or stolen. Encrypting sensitive data at rest adds another roadblock and layer of complexity for the adversary and helps protect customer, employee, and partner data.

## Scope

This control applies to red, orange, and yellow data.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.02_encryption_of_data_at_rest.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.02_encryption_of_data_at_rest.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.4.02_encryption_of_data_at_rest.md).

## Framework Mapping

* ISO
  * A.18.1.4
  * A.18.1.5
  * A.8.2.3
* PCI
  * 3.4
  * 3.5
  * 3.5.3
  * 3.6
  * 3.6.3
  * 8.2.1
