---
layout: markdown_page
title: "VUL.3.01 - Infrastructure Patch Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.3.01 - Infrastructure Patch Management

## Control Statement

GitLab installs security-relevant patches, including software or firmware updates; identified end-of-life software must have a documented decommission plan in place before the software is removed from the environment.

## Context

This control details security best practices in relation to infrastructure patching. This control is trying to ensure that all GitLab productions systems are up to date according to our own patching standards. We need to prove that patching is prioritized and we consistently apply all possible patches and decommission systems as appropriate.

## Scope

This control applies to all GitLab production systems.

## Ownership

Control Owner:

* Infrastructure (.com)

* Security Operations (everything else)

Process Owner:

* Infrastructure (.com)

* Security Operations (everything else)

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.3.01_infrastructure_patch_management.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.3.01_infrastructure_patch_management.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.3.01_infrastructure_patch_management.md).

## Framework Mapping

* SOC2 CC
  * CC7.1
* PCI
  * 6.2
